/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conexion;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

/**
 *
 * @author Carlos
 */
public class UConnection {

    private static Connection con = null;

    public static Connection getConnection() {
        try {
            if (con == null) {
                String url = "jdbc:mysql://localhost:3306/millonario_db";
                String driver = "com.mysql.jdbc.Driver";
                String user = "root";
                String password = "";

                Class.forName(driver);
                con = DriverManager.getConnection(url, user, password);
            }
            return con;
        } catch (ClassNotFoundException | SQLException e) {
            throw new RuntimeException("Error de conexión ", e);

        }
    }

     public static void cerrar(PreparedStatement ps, ResultSet rs, Connection cnn, CallableStatement cs) {
        try {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (cnn != null) {
                cnn.close();
            }
            if (cs != null) {
                cs.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param cnn
     */
    public static void cerrar(Connection cnn) {
        cerrar(null, null, cnn, null);
    }

    /**
     *
     * @param ps
     * @param rs
     */
    public static void cerrar(PreparedStatement ps, ResultSet rs, Connection con) {
        cerrar(ps, rs, con, null);
    }

    /**
     *
     * @param ps
     * @param rs
     */
    public static void cerrar(CallableStatement cs) {
        cerrar(null, null, null, cs);
    }

}
