package controladores;

import DAOs.PreguntaDAO;
import DAOs.RespuestaDAO;
import VO.Pregunta;
import VO.Respuesta;
import java.util.ArrayList;

/**
 *
 * @author Carlos
 */
public class Controller {

    public ArrayList<Pregunta> obtenerPreguntas() throws Exception {

        PreguntaDAO preDao = new PreguntaDAO();
        return preDao.consultar();
    }
    

    public Pregunta obtenerPregunta(int idPregunta) throws Exception {
        
        RespuestaDAO rtaDao = new RespuestaDAO();
        ArrayList<Respuesta> rtas = new ArrayList<>();
        rtas.clear();
        rtas = rtaDao.consultarXPregunta(idPregunta);
        PreguntaDAO preDao = new PreguntaDAO();
        Pregunta pregunta = new Pregunta();
        pregunta = preDao.detalle(idPregunta);
        pregunta.setRespuestas(rtas);
        return pregunta;
    }

    
    public ArrayList<Pregunta> obtenerPreguntasYRespuestas() throws Exception {

        int id, count = 0;
        ArrayList<Pregunta> preguntas = obtenerPreguntas();
        int tamanio = preguntas.size();
        ArrayList<Pregunta> lista = new ArrayList<>();
        while (tamanio > 0) {
            id = preguntas.get(count).getId();
            Pregunta pre = obtenerPregunta(id);
            count++;    
            tamanio--;
            lista.add(pre);
        }
        return lista;
    }

}

