/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOs;

import Conexion.UConnection;
import VO.Respuesta;
import java.sql.*;
import java.util.*;

/**
 *
 * @author Carlos
 */
public class RespuestaDAO {

    public static ResultSet rs = null;
    public ArrayList<Respuesta> lista = new ArrayList<>();
    public static int flag;
//    public  String query;

    public ArrayList<Respuesta> consultar() throws Exception {
        PreparedStatement ps = null;

        try {
            Connection con = UConnection.getConnection();
            String query = "SELECT * FROM respuestas;";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            Respuesta res = null;
            while (rs.next()) {
                res = new Respuesta();
                res.setId(rs.getInt("id"));
                res.setIdPregunta(rs.getInt("idPregunta"));
                res.setName(rs.getString("name"));
                res.setCorrecto(rs.getBoolean("correcto"));
                lista.add(res);
            }
//            UConnection.cerrar(ps, rs, con);
            return lista;

        } catch (SQLException e) {
            throw new Exception("Error de conexxion", e);
        }
    }

    public ArrayList<Respuesta> consultarXPregunta(int id) throws Exception {

        PreparedStatement ps = null;
        try {
            Connection con = UConnection.getConnection();
            String query = "SELECT * FROM respuestas where idPregunta= ?";
            ps = con.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                Respuesta res = new Respuesta();
                res.setId(rs.getInt("id"));
                res.setIdPregunta(rs.getInt("idPregunta"));
                res.setName(rs.getString("name"));
                res.setCorrecto(rs.getBoolean("correcto"));
                lista.add(res);
            }
//            UConnection.cerrar(ps, rs, con);
            return lista;

        } catch (SQLException e) {
            throw new Exception("Error de conexxion", e);
        }
    }

    public static int crear(Respuesta resp) throws Exception {
        PreparedStatement ps = null;

        try {
            Connection con = UConnection.getConnection();
            String query = "INSERT INTO `respuestas`(`idPregunta`,`name`, `correcto`)VALUES(?,?,?);";
            ps = con.prepareStatement(query);
            ps.setInt(1, resp.getIdPregunta());
            ps.setString(2, resp.getName());
            ps.setBoolean(3, resp.getCorrecto());
            flag = ps.executeUpdate();
            if (flag != 1) {
                System.out.println("No se pudo insertar la fila");
            }
            System.out.println("1 fila insertada correctamente");

        } catch (SQLException e) {
            throw new Exception("Error de conexxion");
        }
        return flag;
    }

//    public static Respuesta Detalle(int id) throws Exception {
//        PreparedStatement ps = null;
//
//        try {
//            Connection cnn = DbConection.getConnection();
//            query = "SELECT `respuesta`.`id`,`respuesta`.`idPregunta`,`respuesta`.`nameRespuesta` FROM `respuesta` WHERE id=?;";
//            ps = cnn.prepareStatement(query);
//            ps.setInt(1, id);
//            rs = ps.executeQuery();
//            rs.next();
//            Respuesta resp = new Respuesta(rs.getInt("id"));
//            resp.setIdPregunta(rs.getInt("idPregunta"));
//            resp.setNameRespuestas(rs.getString("nameRespuesta"));
//            return resp;
//
//        } catch (SQLException e) {
//            throw new Exception("Error de conexxion");
//
//        }
//    }
//    public static boolean borrar(int id) {
//        try {
//            Connection con = DbConection.getConnection();
//            query = ""
//
//        } catch (Exception e) {
//        }
//    }
}
