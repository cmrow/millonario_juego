/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOs;

import Conexion.UConnection;
import VO.Pregunta;
import VO.Respuesta;
import java.sql.*;
import java.util.*;

/**
 * ArrayList
 *
 * @author Carlos
 */
public class PreguntaDAO {

    public static ResultSet rs = null;
    public ArrayList<Pregunta> lista = new ArrayList<>();
    public static int flag;
    public static String query;

    public ArrayList<Pregunta> consultar() throws Exception {
        PreparedStatement ps = null;
        try {
            Connection con = UConnection.getConnection();
            query = "SELECT * FROM `millonario_db`.`preguntas`";
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                Pregunta pre = new Pregunta();
                pre.setId(rs.getInt("id"));
                pre.setEnunciado(rs.getString("enunciado"));
                pre.setValor(rs.getInt("valor"));
                lista.add(pre);
            }
//            UConnection.cerrar(ps, rs, con);
            return lista;

        } catch (SQLException e) {
            throw new Exception("Error de conexxion", e);
        }
    }

    public Pregunta detalle(int idPregunta) throws Exception {
        PreparedStatement ps = null;
        try {
            Connection con = UConnection.getConnection();
            query = "SELECT * FROM preguntas WHERE id = ?";
            ps = con.prepareStatement(query);
            ps.setInt(1, idPregunta);
            rs = ps.executeQuery();
            rs.next();
            Pregunta pre = new Pregunta(idPregunta);
            pre.setId(rs.getInt("id"));
            pre.setEnunciado(rs.getString("enunciado"));
            pre.setValor(rs.getInt("valor"));
//                        UConnection.cerrar(ps, rs, con);

            return pre;

        } catch (SQLException e) {
            throw new Exception("Error de conexión", e);
        }
    }

    public static int crear(Pregunta preg) throws Exception {
        PreparedStatement ps = null;

        try {
            Connection con = UConnection.getConnection();
            query = "INSERT INTO `preguntas`(`enunciado`, `valor`)VALUES(?,?);";
            ps = con.prepareStatement(query);
            ps.setString(1, preg.getEnunciado());
            ps.setInt(2, preg.getValor());
            flag = ps.executeUpdate();
            if (flag != 1) {
                System.out.println("No se pudo insertar la fila");
            }
            System.out.println("1 fila insertada correctamente");

        } catch (SQLException e) {
            throw new Exception("Error de conexxion", e);
        }
        return flag;
    }

}
