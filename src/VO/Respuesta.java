/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;


/**
 *
 * @author Carlos
 */
public class Respuesta {

    

    int id;
    int idPregunta;
    String name;
    Boolean correcto;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(int idPregunta) {
        this.idPregunta = idPregunta;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getCorrecto() {
        return correcto;
    }

    public void setCorrecto(Boolean correcto) {
        this.correcto = correcto;
    }

    
    public Respuesta(int id, int idPregunta, String name, Boolean correcto) {
        this.id = id;
        this.idPregunta = idPregunta;
        this.name = name;
        this.correcto = correcto;
    }

    public Respuesta() {
    }

    @Override
    public String toString() {
        return "Respuesta{" + "id=" + id + ", idPregunta=" + idPregunta + ", name=" + name + ", correcto=" + correcto + '}';
    }

   
    
   

    
    
    
}
