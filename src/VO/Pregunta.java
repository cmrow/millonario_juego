/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VO;

import java.util.ArrayList;

/**
 *
 * @author Carlos
 */
public class Pregunta {

    int id;
    String enunciado;
    ArrayList<Respuesta> respuestas;
    int valor;

    public Pregunta(int id) {
        this.id = id;
    }
    

    public Pregunta(int id, String enunciado, ArrayList<Respuesta> respuestas, int valor) {
        this.id = id;
        this.enunciado = enunciado;
        this.respuestas = respuestas;
        this.valor = valor;
    }

    
    
    public Pregunta() {
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEnunciado() {
        return enunciado;
    }

    public void setEnunciado(String enunciado) {
        this.enunciado = enunciado;
    }

    public ArrayList<Respuesta> getRespuestas() {
        return respuestas;
    }

    public void setRespuestas(ArrayList<Respuesta> respuestas) {
        this.respuestas = respuestas;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

     
 

}
