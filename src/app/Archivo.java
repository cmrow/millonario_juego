/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import controladores.Controller;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import DAOs.PreguntaDAO;
import DAOs.RespuestaDAO;
import VO.Pregunta;
import VO.Respuesta;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayList;
import jdk.nashorn.internal.parser.Scanner;

/**
 *
 * @author Carlos
 */
public class Archivo {


    public void escribirDocumento() throws IOException, Exception {

        String fichero = "archivo.txt";
        Writer out = null;

        Controller ctrl = new Controller();
        ArrayList<Pregunta> tanda1 = ctrl.obtenerPreguntasYRespuestas();

        try {
            out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(fichero), "UTF-8"));
            int tamanio = tanda1.size();
            int count = 0;
            while (tamanio > 0) {
                Pregunta pregunta = tanda1.get(count);
                out.write(pregunta.getEnunciado() + "\n");
                for (int i = 0; i < 4; i++) {
                    String rta = pregunta.getRespuestas().get(i).getName();
                    String crr = Boolean.toString(pregunta.getRespuestas().get(i).getCorrecto());
                    out.write(rta + " = " + crr + "\n");
                }
                out.write("\n");
                count++;
                tamanio--;
            }

        } catch (UnsupportedEncodingException | FileNotFoundException ex2) {
            System.out.println("Mensaje error 2: " + ex2.getMessage());
        } finally {
            try {
                out.close();
            } catch (IOException ex3) {
                System.out.println("Mensaje error cierre fichero: " + ex3.getMessage());
            }
        }

    }

//    public void leerDocumento() {
//        String local = "archivo.txt";
//        File fichero = new File(local);
//        Scanner s = null;
//        try {
//            // Leemos el contenido del fichero
//            System.out.println("... Leemos el contenido del fichero ...");
//            s = new Scanner(fichero);
//
//            // Leemos linea a linea el fichero
//            while (s.hasNextLine()) {
//                String linea = s.nextLine(); 	// Guardamos la linea en un String
//                System.out.println(linea);      // Imprimimos la linea
//            }
//
//        } catch (Exception ex) {
//            System.out.println("Mensaje: " + ex.getMessage());
//        } finally {
//            // Cerramos el fichero tanto si la lectura ha sido correcta o no
//            try {
//                if (s != null) {
//                    s.close();
//                }
//            } catch (Exception ex2) {
//                System.out.println("Mensaje 2: " + ex2.getMessage());
//            }
//        }
//    }
}
